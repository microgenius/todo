import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { Todo } from '../todo';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { TodoService } from '../todo.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit',
  standalone: true,
  imports: [CommonModule, RouterModule, ReactiveFormsModule],
  templateUrl: './edit.component.html',
  styleUrl: './edit.component.css'
})
export class EditComponent {
  todoId!: any;
  todo!: Todo;
  form!: FormGroup;
  selectedStatus!: string;
  statusOptions: string[] = ['To Do', 'In Progress', 'Done'];

  constructor(public todoService: TodoService, private router: Router, private route: ActivatedRoute) { 

  }

  toastr = inject(ToastrService)

  ngOnInit():void {
    this.todoId = this.route.snapshot.params['todoId'];
    this.todoService.find(this.todoId).subscribe((data: Todo) => {
      this.todo = data;
    });

    this.form = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      date: new FormControl(new Date(), Validators.required)
    });

    this.selectedStatus = this.todo.status;
  }

  get f(){
    return this.form.controls;
  }

  submit(){
    this.todo.status = this.selectedStatus;
    this.todoService.update(this.todoId, this.form.value).subscribe((res:any) => {
      this.toastr.success("Task updated successfully!", "Info");
      this.router.navigateByUrl('todo/index');
    })
  }

}
