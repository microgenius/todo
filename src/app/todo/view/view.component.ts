import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Todo } from '../todo';
import { TodoService } from '../todo.service';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-view',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './view.component.html',
  styleUrl: './view.component.css'
})
export class ViewComponent {

  todoId!: any;
  todo!: Todo;

  constructor(public todoService:TodoService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit():void {
    this.todoId = this.route.snapshot.params['todoId'];
    this.todoService.find(this.todoId).subscribe((data: Todo) => {
      this.todo = data;
      console.log(this.todo);
    });
  }

}
