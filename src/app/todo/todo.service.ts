import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { Todo } from './todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private apiURL = 'https://task-management-app-vs1x.onrender.com';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json'
    })
  }

  constructor(private httpClient:HttpClient) { }
  //Get all records from the backend
  getAll():Observable<any>{
    return this.httpClient.get(this.apiURL+'/api/tasks/').pipe(catchError((error:
      HttpErrorResponse) => {
        return throwError(error);
      }))
  }

  //Add records to the backend
  create(todo:Todo):Observable<any>{
    return this.httpClient.post(this.apiURL+'/api/tasks/', JSON.stringify(todo), this.httpOptions).pipe(catchError((error:
      HttpErrorResponse) => {
        return throwError(error);
      }))
  }

  //find data wrt to id from the backend
  find(id:any):Observable<any>{
    return this.httpClient.get(this.apiURL+'/api/tasks/'+id).pipe(catchError((error:
      HttpErrorResponse) => {
        return throwError(error);
      }))
  }

  //update record in the backend
  update(id:any, todo:Todo):Observable<any>{
    return this.httpClient.put(this.apiURL+'/api/tasks/'+id, JSON.stringify(todo), this.httpOptions).pipe(catchError((error:
      HttpErrorResponse) => {
        return throwError(error);
      }))
  }

  //delete record from the backend
  delete(id:any){
    return this.httpClient.delete(this.apiURL+'/api/tasks/'+id).pipe(catchError((error:
      HttpErrorResponse) => {
        return throwError(error);
      }))
  }
}
