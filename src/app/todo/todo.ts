export interface Todo {
    _id:any;
    title:string;
    description:string;
    status:string;
    date: Date;
}
