import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { Todo } from '../todo';
import { TodoService } from '../todo.service';
import { LoadingSpinnerComponent } from '../loading-spinner/loading-spinner.component';

@Component({
  selector: 'app-index',
  standalone: true,
  imports: [CommonModule, RouterModule, LoadingSpinnerComponent],
  templateUrl: './index.component.html',
  styleUrl: './index.component.css'
})
export class IndexComponent {

  isLoading: boolean = true;

  todos:Todo[]=[];

  constructor(public todoService:TodoService, private router: Router){}

  navigateToView(todo:Todo){
    this.router.navigate(['/todo/', todo._id, 'view']);
  }

  navigateToEdit(todo:Todo){
    this.router.navigate(['/todo/', todo._id, 'edit']);
  }

  ngOnInit():void{
    this.isLoading = true;
    this.todoService.getAll().subscribe((data:Todo[]) =>{
      this.todos = data;
      this.isLoading = false;
    })
  }

  deleteTodo(todoId:any){
    this.todoService.delete(todoId).subscribe(res =>{
      this.todos = this.todos.filter(todo => todo._id !== todoId);
    })
  }

}
