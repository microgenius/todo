import { Routes } from '@angular/router';
import { IndexComponent } from './todo/index/index.component';
import { CreateComponent } from './todo/create/create.component';
import { EditComponent } from './todo/edit/edit.component';
import { ViewComponent } from './todo/view/view.component';

export const routes: Routes = [
    {path:'', redirectTo:'todo/index', pathMatch:'full'},
    {path:'todo/index', component:IndexComponent},
    {path:'todo/create', component:CreateComponent},
    {path:'todo/:todoId/edit', component:EditComponent},
    {path:'todo/:todoId/view', component:ViewComponent}
];
